package vboot.extend.oa.flow.cate;

import vboot.core.common.mvc.entity.BaseCateEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class OaFlowCate extends BaseCateEntity {

    @ManyToOne(fetch= FetchType.EAGER)
    @JoinColumn(name = "pid")
    private OaFlowCate parent;//父类别

//    @Transient
//    private List<OaFlowCate> children = new ArrayList<>(); //父流程分类

}
