package vboot.extend.oa.flow.rece;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Table(indexes = {@Index(columnList = "useid"),@Index(columnList = "floid")})
public class OaFlowRece {

    @Id
    @Column(length = 32)
    private String id;//ID

    @Column(length = 32)
    private String useid;//用户ID

    @Column(length = 32)
    private String floid;//最近使用的流程ID

    @Column
    private Date uptim = new Date();//最近使用时间
}
