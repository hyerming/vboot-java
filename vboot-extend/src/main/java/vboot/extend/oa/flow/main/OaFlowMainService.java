package vboot.extend.oa.flow.main;

import vboot.core.common.mvc.service.BaseMainService;
import vboot.core.module.bpm.proc.main.BpmProcMainService;
import vboot.core.module.bpm.proc.main.Zbpm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import vboot.core.module.bpm.proc.main.Znode;
import vboot.core.common.utils.web.XuserUtil;
import vboot.core.common.utils.lang.IdUtils;

import javax.annotation.PostConstruct;
import java.util.Date;

@Service
public class OaFlowMainService extends BaseMainService<OaFlowMain> {

    public void insert1(OaFlowMain oaFlowMain) {
        oaFlowMain.setState("20");
        oaFlowMain.setId(IdUtils.getUID());
        oaFlowMain.setUptim(new Date());
        if (oaFlowMain.getCrman() == null) {
            oaFlowMain.setCrman(XuserUtil.getUser());
        }
        repo.saveAndFlush(oaFlowMain);
//        OaFlowMain dbMain = repo.saveAndFlush(oaFlowMain);

    }

    public void insert2(OaFlowMain main) throws Exception {
        Zbpm zbpm = main.getZbpm();
        zbpm.setProid(main.getId());
        zbpm.setProna(main.getName());
        zbpm.setHaman(main.getCrman().getId());
        zbpm.setTemid(main.getProtd());
        Znode znode = procService.start(zbpm);
        if ("NE".equals(znode.getFacno())) {
            main.setState("30");
            super.update(main);
        }
    }

//    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void insertx(OaFlowMain oaFlowMain) throws Exception {
        oaFlowMain.setState("20");
        oaFlowMain.setId(IdUtils.getUID());
        oaFlowMain.setUptim(new Date());
        if (oaFlowMain.getCrman() == null) {
            oaFlowMain.setCrman(XuserUtil.getUser());
        }
//        OaFlowMain dbMain = repo.save(oaFlowMain);
        OaFlowMain dbMain = repo.saveAndFlush(oaFlowMain);

        Zbpm zbpm = oaFlowMain.getZbpm();
        zbpm.setModty("oaFlow");
        zbpm.setProid(dbMain.getId());
        zbpm.setProna(dbMain.getName());
        zbpm.setHaman(dbMain.getCrman().getId());
        zbpm.setTemid(dbMain.getProtd());
        Znode znode = procService.start(zbpm);
        if ("NE".equals(znode.getFacno())) {
            dbMain.setState("30");
            super.update(dbMain);
        }
    }

    public void updatex(OaFlowMain oaFlowMain) throws Exception {
        oaFlowMain.setState("20");
        super.update(oaFlowMain);
        repo.flush();
        oaFlowMain.getZbpm().setModty("oaFlow");
        if ("pass".equals(oaFlowMain.getZbpm().getOpkey())) {
            Znode znode = procService.handlerPass(oaFlowMain.getZbpm());
            if ("NE".equals(znode.getFacno())) {
                oaFlowMain.setState("30");
                super.update(oaFlowMain);
            }else{
                oaFlowMain.setState("20");
                super.update(oaFlowMain);
            }
        } else if ("refuse".equals(oaFlowMain.getZbpm().getOpkey())) {
            if ("N1".equals(oaFlowMain.getZbpm().getTarno())) {
                oaFlowMain.setState("11");
                super.update(oaFlowMain);
            }
            Znode znode = procService.handlerRefuse(oaFlowMain.getZbpm());
        } else if ("turn".equals(oaFlowMain.getZbpm().getOpkey())) {
            procService.handlerTurn(oaFlowMain.getZbpm());
        } else if ("communicate".equals(oaFlowMain.getZbpm().getOpkey())) {
            procService.handlerCommunicate(oaFlowMain.getZbpm());
        } else if ("abandon".equals(oaFlowMain.getZbpm().getOpkey())) {
            procService.handlerAbandon(oaFlowMain.getZbpm());
            oaFlowMain.setState("00");
            super.update(oaFlowMain);
        }else if ("bacommunicate".equals(oaFlowMain.getZbpm().getOpkey())) {
            procService.handlerBacommunicate(oaFlowMain.getZbpm());
        }else if ("cacommunicate".equals(oaFlowMain.getZbpm().getOpkey())) {
            procService.handlerCacommunicate(oaFlowMain.getZbpm());
        }
    }

    //----------bean注入------------
    @Autowired
    private BpmProcMainService procService;

    @Autowired
    private OaFlowMainRepo repo;

    @PostConstruct
    public void initDao() {
        super.setRepo(repo);
    }

}
