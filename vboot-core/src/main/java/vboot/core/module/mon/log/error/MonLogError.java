package vboot.core.module.mon.log.error;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Getter
@Setter
@NoArgsConstructor
@ApiModel("异常日志")
public class MonLogError implements Serializable {

    @Id
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 32)
    @ApiModelProperty("操作名称")
    private String name;

    @Column(length = 32)
    @ApiModelProperty("用户ID")
    private String useid;

    @ApiModelProperty("方法名")
    private String method;

    @Lob
    @ApiModelProperty("参数")
    private String param;

    @ApiModelProperty("日志类型")
    private String type;

    @ApiModelProperty("请求ip")
    private String ip;

    @ApiModelProperty("地址")
    private String addre;

    @ApiModelProperty("浏览器")
    private String agbro;

    @ApiModelProperty("操作系统")
    private String ageos;

    @ApiModelProperty("请求耗时")
    private Long time;

    @Lob
    @ApiModelProperty("异常详细")
    private String error;

    @CreationTimestamp
    @ApiModelProperty("创建时间")
    private Timestamp crtim;

    public MonLogError(String type, Long time) {
        this.type = type;
        this.time = time;
    }
}
