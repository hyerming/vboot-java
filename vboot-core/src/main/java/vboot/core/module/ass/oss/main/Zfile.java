package vboot.core.module.ass.oss.main;

import lombok.Data;

@Data
public class Zfile {

    private String id;

    private String name;

    private String size;

    private String path;

    private String filid;

}
