package vboot.core.module.ass.addr.prov;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@ApiModel("省份信息")
public class AssAddrProv extends BaseEntity {

    @Column(length = 32)
    @ApiModelProperty("坐标")
    private String cecoo;
}
