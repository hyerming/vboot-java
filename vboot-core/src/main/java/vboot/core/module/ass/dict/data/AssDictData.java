package vboot.core.module.ass.dict.data;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
@Getter
@Setter
@ApiModel("字典数据")
public class AssDictData extends BaseMainEntity {

    @Column(length = 32)
    @ApiModelProperty("数据编码")
    private String code;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @Column(length = 32)
    @ApiModelProperty("字典ID")
    private String maiid;
}