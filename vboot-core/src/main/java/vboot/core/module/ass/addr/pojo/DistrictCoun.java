package vboot.core.module.ass.addr.pojo;

import lombok.Data;

import java.util.List;

@Data
public class DistrictCoun {

    private String adcode;

    private String name;

    private String center;

    private String level;

    private List<DistrictProv> districts;

}
