package vboot.core.module.ass.dict.main;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("ass/dict/main")
@Api(tags = {"字典信息"})
public class AssDictMainApi {

    private String table = "ass_dict_main";

    @GetMapping
    @ApiOperation("查询字典主信息分页")
    public R get(String name) {
        Sqler sqler = new Sqler(table);
        if(StrUtil.isNotBlank(name)){
            sqler.addWhere("(t.name like ? or t.id like ?)","%"+name+"%","%"+name+"%");
        }
        sqler.addSelect("t.notes,t.ornum");
        sqler.addOrder("t.ornum");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询字典主信息详情")
    public R getOne(@PathVariable String id) {
        AssDictMain main = service.findOne(id);
        return R.ok(main);
    }

    @GetMapping("list")
    @ApiOperation("查询字典主信息列表")
    public R getList() {
        return R.ok(service.findAll());
    }

    @PostMapping
    @ApiOperation("新增字典")
    public R post(@RequestBody AssDictMain main) throws DocumentException {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改字典")
    public R put(@RequestBody AssDictMain main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除字典")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private AssDictMainService service;

}
