package vboot.core.module.ass.dict.data;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("ass/dict/data")
@Api(tags = {"字典数据"})
public class AssDictDataApi {

    private String table = "ass_dict_data";

    @GetMapping
    @ApiOperation("查询字典数据分页")
    public R get(String name, String maiid) {
        Sqler sqler = new Sqler(table);
        if(StrUtil.isNotBlank(name)){
            sqler.addWhere("(t.name like ? or t.id like ?)","%"+name+"%","%"+name+"%");
        }
        sqler.addEqual("t.maiid", maiid);
        sqler.addSelect("t.notes,t.ornum,t.code");
        sqler.addOrder("t.ornum");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("list")
    @ApiOperation("查询字典数据列表")
    public R getList(String maiid) {
        Sqler sqler = new Sqler(table);
        sqler.addEqual("t.maiid",maiid);
        sqler.addOrder("t.ornum");
        return R.ok( jdbcDao.findIdNameList(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询字典数据详情")
    public R getOne(@PathVariable String id) {
        AssDictData main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增字典数据")
    public R post(@RequestBody AssDictData main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改字典数据")
    public R put(@RequestBody AssDictData main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除字典数据")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private AssDictDataService service;

    @Autowired
    private JdbcDao jdbcDao;

}
