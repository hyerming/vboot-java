package vboot.core.module.ass.dict.main;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;

import javax.persistence.Entity;

@Entity
@Getter
@Setter
@ApiModel("字典信息")
public class AssDictMain extends BaseMainEntity {

    @ApiModelProperty("排序号")
    private Integer ornum;

}