package vboot.core.module.sys.todo.main;

import vboot.core.module.bpm.proc.main.Zbpm;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.module.bpm.task.main.BpmTaskMain;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.module.sys.org.root.SysOrgRepo;
import vboot.core.module.sys.todo.done.SysTodoDone;
import vboot.core.module.sys.todo.done.SysTodoDoneRepo;
import vboot.core.module.sys.todo.user.SysTodoUser;
import vboot.core.module.sys.todo.user.SysTodoUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import vboot.core.common.utils.lang.IdUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
@Transactional(rollbackFor = Exception.class)
public class SysTodoMainService {

    public void sendTodos(Zbpm zbpm, List<BpmTaskMain> taskList) {
        for (BpmTaskMain task : taskList) {
            if (task.getActag()) {
                SysTodoMain todo = new SysTodoMain();
//                todo.setId(IdUtils.getUID());
                todo.setId(task.getId());
                todo.setName(zbpm.getProna());
                todo.setLink("/#/page/ofmv?id=" + zbpm.getProid());
                todo.setModid(zbpm.getProid());
                repo.save(todo);

                SysOrg sysOrg = sysOrgRepo.findById(task.getExman()).get();
                if (sysOrg.getType() == 8) {
                    SysTodoUser todoTarget = new SysTodoUser();
                    todoTarget.setId(IdUtils.getUID());
                    todoTarget.setTodid(todo.getId());
                    todoTarget.setUseid(task.getExman());
                    targetRepo.save(todoTarget);
                } else if (sysOrg.getType() == 4) {
                    String sql = "select oid id from sys_org_post_org where pid=?";
                    List<String> userIdList = jdbcDao.findStringList(sql, task.getExman());
                    for (String uid : userIdList) {
                        SysTodoUser todoTarget = new SysTodoUser();
                        todoTarget.setId(IdUtils.getUID());
                        todoTarget.setTodid(todo.getId());
                        todoTarget.setUseid(uid);
                        targetRepo.save(todoTarget);
                    }
                }
            }
        }
    }

//    public void sendTodo(Zbpm zbpm, Znode znode) {
//        SysTodoMain todo = new SysTodoMain();
//        todo.setId(IdUtils.getUID());
//        todo.setName(zbpm.getProna());
//        todo.setLink("/#/page/ofmv?id=" + zbpm.getProid());
//        todo.setModid(zbpm.getProid());
//
//
//        SysTodoUser todoTarget = new SysTodoUser();
//        todoTarget.setId(IdUtils.getUID());
//        todoTarget.setTodid(todo.getId());
//        todoTarget.setUseid(znode.getExmen());
//        repo.save(todo);
//        targetRepo.save(todoTarget);
//    }

    public void sendTodo(Zbpm zbpm,String useid) {
        SysTodoMain todo = new SysTodoMain();
        todo.setId(IdUtils.getUID());
        todo.setName(zbpm.getProna());
        todo.setLink("/#/page/ofmv?id=" + zbpm.getProid());
        todo.setModid(zbpm.getProid());


        SysTodoUser todoTarget = new SysTodoUser();
        todoTarget.setId(IdUtils.getUID());
        todoTarget.setTodid(todo.getId());
        todoTarget.setUseid(useid);
        repo.save(todo);
        targetRepo.save(todoTarget);
    }

//    public void doneTodo(Zbpm zbpm) {
//        String sql = "select m.id,t.id as tid from sys_todo_main m inner join sys_todo_user t on t.todid=m.id " +
//                "where t.useid=? and m.modid=?";
//        Map<String, Object> map = jdbcDao.findMap(sql, zbpm.getHaman(), zbpm.getProid());
//        String delsql = "delete from sys_todo_user where id=?";
//        jdbcDao.update(delsql, map.get("tid"));
//
//        SysTodoDone done = new SysTodoDone();
//        done.setId(IdUtils.getUID());
//        done.setTodid((String)map.get("id"));
//        done.setUseid(zbpm.getHaman());
//        doneRepo.save(done);
//    }

    public void doneTodo(Zbpm zbpm) {
        String sql = "select t.useid \"useid\",m.id \"todid\",t.id \"tarid\" from sys_todo_main m inner join sys_todo_user t on t.todid=m.id " +
                "where m.modid=?";
        List<Map<String, Object>> mapList = jdbcDao.findMapList(sql, zbpm.getProid());
        for (Map<String, Object> map : mapList) {
            if (zbpm.getHaman().equals(map.get("useid"))) {

                String delsql = "delete from sys_todo_user where todid=?";
                jdbcDao.update(delsql, map.get("todid"));

                SysTodoDone done = new SysTodoDone();
                done.setId(IdUtils.getUID());
                done.setTodid((String)map.get("todid"));
                done.setUseid(zbpm.getHaman());
                doneRepo.save(done);
            }

        }
    }

    public void doneTodos(Zbpm zbpm) {
        String sql = "select t.useid \"useid\",m.id \"todid\",t.id \"tarid\" from sys_todo_main m inner join sys_todo_user t on t.todid=m.id " +
                "where m.modid=?";
        List<Map<String, Object>> mapList = jdbcDao.findMapList(sql, zbpm.getProid());
        for (Map<String, Object> map : mapList) {
            String delsql = "delete from sys_todo_user where id=?";
            jdbcDao.update(delsql, map.get("tarid"));
            if (zbpm.getHaman().equals(map.get("useid"))) {
                SysTodoDone done = new SysTodoDone();
                done.setId(IdUtils.getUID());
                done.setTodid((String)map.get("todid"));
                done.setUseid(zbpm.getHaman());
                doneRepo.save(done);
            }
        }
    }

    public void doneTodosByTaskIds(String[] taskIds) {
        for (String taskId : taskIds) {
            String delsql = "delete from sys_todo_main where id=?";
            jdbcDao.update(delsql, taskId);

            String delsql2 = "delete from sys_todo_user where todid=?";
            jdbcDao.update(delsql2, taskId);
        }
    }

    public String insert(SysTodoMain main) {
        if (main.getId() == null || "".equals(main.getId())) {
            main.setId(IdUtils.getUID());
        }
        main.setCrtim(new Date());
        repo.save(main);
        return main.getId();
    }


    public String update(SysTodoMain main) {
        repo.save(main);
        return main.getId();
    }

    public int delete(String[] ids) {
        for (String id : ids) {
            repo.deleteById(id);
        }
        return ids.length;
    }

    @Transactional(readOnly = true)
    public SysTodoMain findOne(String id) {
        return repo.findById(id).get();
    }


    //----------bean注入------------
    @Autowired
    private JdbcDao jdbcDao;

    @Autowired
    private SysOrgRepo sysOrgRepo;

    @Autowired
    private SysTodoMainRepo repo;

    @Autowired
    private SysTodoUserRepo targetRepo;

    @Autowired
    private SysTodoDoneRepo doneRepo;

}
