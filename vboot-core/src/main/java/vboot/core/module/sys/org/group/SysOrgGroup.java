package vboot.core.module.sys.org.group;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import vboot.core.module.sys.org.root.SysOrg;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Data
@ApiModel("组织架构群组")
public class SysOrgGroup {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 100)
    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("备注")
    private String notes;

    @ApiModelProperty("分类ID")
    @Column(length = 32)
    private String catid;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @ApiModelProperty("可用标记")
    private Boolean avtag;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim=new Date();

    @ApiModelProperty("更新时间")
    private Date uptim;

    @ManyToMany
    @JoinTable(name = "sys_org_group_org", joinColumns = {@JoinColumn(name = "gid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("成员信息")
    private List<SysOrg> members;

    @Column(length = 32)
    @ApiModelProperty("标签")
    private String label;//主要用于区分组织架构分类，ABC为系统内置组织架构

}
