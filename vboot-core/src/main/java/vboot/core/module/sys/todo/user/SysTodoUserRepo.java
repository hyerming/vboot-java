package vboot.core.module.sys.todo.user;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SysTodoUserRepo extends JpaRepository<SysTodoUser,String> {


}
