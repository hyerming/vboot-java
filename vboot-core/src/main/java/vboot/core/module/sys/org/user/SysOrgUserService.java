package vboot.core.module.sys.org.user;

import cn.hutool.core.util.StrUtil;
import vboot.core.common.utils.lang.SecureUtils;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.module.sys.org.root.SysOrgRepo;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.api.PageData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;
import vboot.core.common.utils.lang.IdUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@Transactional(rollbackFor = Exception.class)
public class SysOrgUserService {


    @Transactional(readOnly = true)
    public PageData findPageData(Sqler sqler) {
        return jdbcDao.findPageData(sqler);
    }

    public void insertAll(List<SysOrgUser> list) {
        List<SysOrg> orgList = new ArrayList<>();
        for (SysOrgUser main : list) {
            if (main.getId() == null || "".equals(main.getId())) {
                main.setId(IdUtils.getUID());
            }
            if(StrUtil.isBlank(main.getTier())){
                if (main.getDept() == null || StrUtil.isBlank(main.getDept().getId())) {
                    main.setTier("x" + main.getId() + "x");
                } else {
                    String tier = jdbcDao.findOneString("select tier from sys_org_dept where id=?", main.getDept().getId());
                    main.setTier(tier + main.getId() + "x");
                }
            }
            main.setPacod(SecureUtils.passwordEncrypt(main.getPacod()));
            SysOrg sysOrg = new SysOrg(main.getId(), main.getName(),8);
            orgList.add(sysOrg);
        }
        orgRepo.saveAll(orgList);
        userRepo.saveAll(list);
    }


    public String insert(SysOrgUser main) {
        if (main.getId() == null || "".equals(main.getId())) {
            main.setId(IdUtils.getUID());
        }
        if(StrUtil.isBlank(main.getTier())) {
            if (main.getDept() == null || StrUtil.isBlank(main.getDept().getId())) {
                main.setTier("x" + main.getId() + "x");
            } else {
                String tier = jdbcDao.findOneString("select tier from sys_org_dept where id=?", main.getDept().getId());
                main.setTier(tier + main.getId() + "x");
            }
        }
        main.setPacod(SecureUtils.passwordEncrypt(main.getPacod()));
        SysOrg sysOrg = new SysOrg(main.getId(), main.getName(),8);
        orgRepo.save(sysOrg);
        userRepo.save(main);
        return main.getId();
    }


    public String update(SysOrgUser main) {
        main.setUptim(new Date());
        SysOrg sysOrg = new SysOrg(main.getId(), main.getName(),8);
        if (main.getDept() == null || StrUtil.isBlank(main.getDept().getId())) {
            main.setTier("x" + main.getId() + "x");
        } else {
            String tier = jdbcDao.findOneString("select tier from sys_org_dept where id=?", main.getDept().getId());
            main.setTier(tier + main.getId() + "x");
        }
         userRepo.save(main);
        orgRepo.save(sysOrg);
        return main.getId();
    }

    public int delete(String[] ids) {
        for (String id : ids) {
            userRepo.deleteById(id);
            orgRepo.deleteById(id);
        }
        return ids.length;
    }

    public void pacod(PacodPo po) {
        po.setPacod(SecureUtils.passwordEncrypt(po.getPacod()));
        String sql="update sys_org_user set pacod=? where id=?";
        jdbcDao.update(sql,po.getPacod(),po.getId());
    }

    @Transactional(readOnly = true)
    public SysOrgUser findById(String id) {
        return userRepo.findById(id).get();
    }


    @Autowired
    private SysOrgRepo orgRepo;

    @Autowired
    private SysOrgUserRepo userRepo;

    @Autowired
    private JdbcDao jdbcDao;

}
