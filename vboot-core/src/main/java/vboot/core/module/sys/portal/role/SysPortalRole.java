package vboot.core.module.sys.portal.role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;
import vboot.core.module.sys.org.root.SysOrg;
import vboot.core.module.sys.portal.menu.SysPortalMenu;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@ApiModel("门户角色")
public class SysPortalRole extends BaseMainEntity
{

    @Column(length = 32)
    @ApiModelProperty("门户ID")
    private String porid;

    @Column(length = 32)
    @ApiModelProperty("类型")
    private String type;

    @Column(length = 32)
    @ApiModelProperty("标签")
    private String label;

    @ManyToMany
    @JoinTable(name = "sys_portal_role_menu",
            joinColumns = {@JoinColumn(name = "rid")},
            inverseJoinColumns = {@JoinColumn(name = "mid")})
    @ApiModelProperty("拥有的菜单")
    private List<SysPortalMenu> menus  = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "sys_portal_role_org",
            joinColumns = {@JoinColumn(name = "rid")},
            inverseJoinColumns = {@JoinColumn(name = "oid")})
    @ApiModelProperty("授权的用户")
    private List<SysOrg> orgs = new ArrayList<>();

}
