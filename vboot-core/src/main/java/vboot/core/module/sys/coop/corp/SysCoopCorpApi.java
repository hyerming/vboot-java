package vboot.core.module.sys.coop.corp;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;

import java.util.List;

@RestController
@RequestMapping("sys/coop/corp")
@Api(tags = {"外部协同-外部公司"})
public class SysCoopCorpApi {

    @GetMapping
    @ApiOperation("查询分页")
    public R get(String name,String catid) {
        Sqler sqler = new Sqler("sys_coop_corp");
        sqler.addLike("t.name" , name);
        sqler.addEqual("t.catid" , catid);
        sqler.addLeftJoin("c.name catna","sys_coop_cate c" , "c.id=t.catid");
        sqler.addOrder("t.ornum");
        sqler.addSelect("t.notes");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("tree")
    public R getTree() {
        List<Ztree> list = service.findTreeList();
        return R.ok(list);
    }

    @GetMapping("list")
    @ApiOperation("查询列表")
    public R getList() {
        return R.ok(service.findAll());
    }


    @GetMapping("one/{id}")
    @ApiOperation("查询详情")
    public R getOne(@PathVariable String id) {
        SysCoopCorp main = service.findOne(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增")
    public R post(@RequestBody SysCoopCorp main) {
        return R.ok(service.insertx(main));
    }

    @PutMapping
    @ApiOperation("修改")
    public R put(@RequestBody SysCoopCorp main) {
        return R.ok(service.updatex(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.deletex(ids));
    }

    @Autowired
    private SysCoopCorpService service;

}
