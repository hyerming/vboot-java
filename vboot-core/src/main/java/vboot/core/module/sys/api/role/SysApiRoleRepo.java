package vboot.core.module.sys.api.role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SysApiRoleRepo extends JpaRepository<SysApiRole,String> {

}
