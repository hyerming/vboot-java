package vboot.core.module.sys.portal.role;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SysPortalRoleRepo extends JpaRepository<SysPortalRole,String> {
}
