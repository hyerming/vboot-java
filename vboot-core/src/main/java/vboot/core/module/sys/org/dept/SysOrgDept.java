package vboot.core.module.sys.org.dept;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import vboot.core.module.sys.org.root.SysOrg;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(indexes = {@Index(columnList = "tier"), @Index(columnList = "avtag")})
@ApiModel("组织架构部门")
public class SysOrgDept {

    @Id
    @Column(length = 32)
    @ApiModelProperty("主键ID")
    private String id;

    @Column(length = 100)
    @ApiModelProperty("名称")
    private String name;

    @Transient
    @ApiModelProperty("子部门")
    private List<SysOrgDept> children = new ArrayList<>();

    @Transient
    @ApiModelProperty("父ID")
    private String pid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "pid")
    @ApiModelProperty("父部门")
    private SysOrg parent;

    @ApiModelProperty("部门类型：1为机构,2为部门")
    private Integer type;//1为机构,2为部门,4为岗位,8为用户,16为常用群组,32为角色线

    @Column(length = 1000)
    @ApiModelProperty("层级")
    private String tier;

    @Column(length = 32)
    @ApiModelProperty("标签")
    private String label;//主要用于区分组织架构分类，ABC为系统内置组织架构

    @ApiModelProperty("备注")
    private String notes;

    @ApiModelProperty("排序号")
    private Integer ornum;

    @ApiModelProperty("可用标记")
    private Boolean avtag;

    @Column(length = 1000)
    @ApiModelProperty("ldap层级名称")
    private String ldnam;

    @Column(updatable = false)
    @ApiModelProperty("创建时间")
    private Date crtim = new Date();

    @ApiModelProperty("更新时间")
    private Date uptim;

//    @Column(length = 32)
//    private String tecod;// 销售组代码

//    @Transient
//    private String cid;//公司ID

//    @ManyToOne(fetch=FetchType.EAGER)
//    @JoinColumn(name = "cid")
//    private SysOrg corp;//公司

}
