package vboot.core.module.sys.todo.main;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("sys/todo/main")
@Api(tags = {"消息管理-待办信息"})
public class SysTodoMainApi {

    @GetMapping
    @ApiOperation("查询待办分页")
    public R get(String name) {
        Sqler sqler = new Sqler("sys_todo_main");
        sqler.addLike("t.name", name);
        sqler.addOrder("t.crtim desc");
        sqler.addSelect("t.link,t.crtim");
        sqler.addInnerJoin("","sys_todo_user t2", "t2.todid=t.id");
        sqler.addInnerJoin("o.name as exman","sys_org o", "o.id=t2.useid");
        System.out.println(sqler.getSql());
        return R.ok(jdbcDao.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询待办详情")
    public R getOne(@PathVariable String id) {
        SysTodoMain main = service.findOne(id);
        return R.ok(main);
    }


    @PostMapping
    @ApiOperation("新增待办")
    public R post(@RequestBody SysTodoMain main) throws DocumentException {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改待办")
    public R put(@RequestBody SysTodoMain main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除待办")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @Autowired
    private SysTodoMainService service;

    @Autowired
    private JdbcDao jdbcDao;

}
