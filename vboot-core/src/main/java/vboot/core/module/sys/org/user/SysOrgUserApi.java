package vboot.core.module.sys.org.user;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.Sqler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("sys/org/user")
@Api(tags = {"组织架构-用户管理"})
public class SysOrgUserApi {

    private String table = "sys_org_user";

    @GetMapping
    @ApiOperation("查询用户分页")
    public R get(String depid, String name, String usnam, String ninam, String monum) {
        Sqler sqler = new Sqler(table);
        if (StrUtil.isNotBlank(name)|| StrUtil.isNotBlank(usnam)|| StrUtil.isNotBlank(ninam)|| StrUtil.isNotBlank(monum)) {
            sqler.addLike("t.name", name);
            sqler.addLike("t.usnam", usnam);
            sqler.addLike("t.ninam", ninam);
            sqler.addLike("t.monum", monum);
        } else if (StrUtil.isNotBlank(depid)){
            sqler.addEqual("t.depid", depid);
        }
        sqler.addSelect("t.crtim,t.uptim,t.depid,t.notes");
        sqler.addOrder("t.ornum");
        return R.ok(service.findPageData(sqler));
    }

    @GetMapping("one/{id}")
    @ApiOperation("查询用户详情")
    public R getOne(@PathVariable String id, HttpServletRequest request) {
        SysOrgUser main = service.findById(id);
        return R.ok(main);
    }

    @PostMapping
    @ApiOperation("新增用户")
    public R post(@RequestBody SysOrgUser main) {
        return R.ok(service.insert(main));
    }

    @PutMapping
    @ApiOperation("修改用户")
    public R put(@RequestBody SysOrgUser main) {
        return R.ok(service.update(main));
    }

    @DeleteMapping("{ids}")
    @ApiOperation("删除用户")
    public R delete(@PathVariable String[] ids) {
        return R.ok(service.delete(ids));
    }

    @PostMapping("pacod")
    @ApiOperation("修改密码")
    public R pacod(@RequestBody PacodPo po) {
        service.pacod(po);
        return R.ok();
    }

    @Autowired
    private SysOrgUserService service;

}
