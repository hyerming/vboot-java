package vboot.core.module.sys.org.role;

import lombok.Getter;
import lombok.Setter;
import vboot.core.common.mvc.entity.BaseMainEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

//层级树
@Entity
@Getter
@Setter
public class SysOrgRoleTree extends BaseMainEntity {

    private Integer ornum;//排序号

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "treid")
    @OrderBy("ornum ASC")
    private List<SysOrgRole> roles = new ArrayList<>();//层级角色


}
