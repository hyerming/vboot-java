package vboot.core.module.gen.user;

import io.swagger.annotations.ApiOperation;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import vboot.core.common.mvc.api.R;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.pojo.Ztree;
import vboot.core.common.utils.web.XuserUtil;
import vboot.core.framework.security.pojo.Zuser;
import vboot.core.module.ass.oss.main.AssOssMainService;
import vboot.core.module.ass.oss.main.Zfile;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("gen/user")
public class GenUserApi {

    //查询角色树
    @GetMapping("info")
    public R getInfo(Authentication auth) {
        Zuser zuser = (Zuser) auth.getPrincipal();
        if("org".equals(zuser.getLabel())){
            Sqler sqler = new Sqler("t.*","sys_org_user");
            sqler.addEqual("t.id",zuser.getId());
            Map<String, Object> map = jdbcDao.findMap(sqler);
            return R.ok(map);
        }else{
            return R.ok();
        }
    }

    @PostMapping("upload")
    @ApiOperation("头像上传")
    public R upload(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException, NoSuchAlgorithmException {
        Zfile zfile = service.uploadFile(file);
        String sql="update sys_org_user set avsrc=?,avimg=? where id=?";
        jdbcDao.update(sql,zfile.getId(),zfile.getId(),XuserUtil.getUserId());
        return R.ok(zfile);
    }

    @PostMapping("clip")
    @ApiOperation("头像上传")
    public R clip(@RequestParam(value = "file", required = false) MultipartFile file) throws IOException, NoSuchAlgorithmException {
        Zfile zfile = service.uploadFile(file);
        String sql="update sys_org_user set avimg=? where id=?";
        jdbcDao.update(sql,zfile.getId(),XuserUtil.getUserId());
        return R.ok(zfile);
    }

//    @PostMapping("clip")
//    @ApiOperation("头像裁剪")
//    public R clip(String img) {
//        String sql="update sys_org_user set avimg=? where id=?";
//        jdbcDao.update(sql,img,XuserUtil.getUserId());
//        return R.ok();
//    }

    @Autowired
    private AssOssMainService service;


    @Autowired
    private JdbcDao jdbcDao;

}