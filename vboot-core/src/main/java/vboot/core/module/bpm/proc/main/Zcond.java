package vboot.core.module.bpm.proc.main;

import lombok.Data;

@Data
public class Zcond {

    private String proid;//流程实例ID

    private String temid;//流程模板ID

    private String modty;//类别

//    private boolean sttag=false;//开始标记
}
