package vboot.core.common.mvc.service;

import vboot.core.common.mvc.api.PageData;
import vboot.core.common.mvc.dao.JdbcDao;
import vboot.core.common.mvc.dao.Sqler;
import vboot.core.common.mvc.entity.BaseMainEntity;
import vboot.core.common.utils.web.XuserUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;
import vboot.core.common.utils.lang.IdUtils;

import java.util.Date;
import java.util.List;
import java.util.Map;

//主数据Service基类，提供主数据Entity增删改查的通用方法
@Transactional(rollbackFor = Exception.class)
public abstract class BaseMainService<T extends BaseMainEntity> {

    //---------------------------------------查询-------------------------------------
    //查询分页
    @Transactional(readOnly = true)
    public PageData findPageData(Sqler sqler) {
        if (sqler.getAutoType() == 1) {
            sqler.selectCUinfo();
            sqler.addOrder("t.crtim desc");
        }
        return jdbcDao.findPageData(sqler);
    }

    //查询MapList数据
    @Transactional(readOnly = true)
    public List<Map<String, Object>> findMapList(Sqler sqler) {
        return jdbcDao.findMapList(sqler);
    }

    //根据ID判断数据库实体是否存在
    @Transactional(readOnly = true)
    public boolean existsById(String id) {
        return repo.existsById(id);
    }

    //查询单个实体详细信息
    @Transactional(readOnly = true)
    public T findOne(String id) {
        return repo.findById(id).get();
    }

    //查询所有记录
    @Transactional(readOnly = true)
    public List<T> findAll() {
        return repo.findAll();
    }

    //---------------------------------------增删改-------------------------------------
    //新增
    public String insert(T main) {
        if (main.getId() == null || "".equals(main.getId())) {
            main.setId(IdUtils.getUID());
        }
        main.setCrtim(new Date());
        if (main.getCrman() == null) {
            main.setCrman(XuserUtil.getUser());
        }
        repo.save(main);
        return main.getId();
    }

    //修改
    public String update(T main) {
        main.setUptim(new Date());
        if (main.getUpman() == null) {
            main.setUpman(XuserUtil.getUser());
        }
        repo.save(main);
        return main.getId();
    }

    //删除
    public int delete(String[] ids) {
        for (String id : ids) {
            repo.deleteById(id);
        }
        return ids.length;
    }

    //新增或修改
    public T save(T t) {
        return repo.save(t);
    }


    //---------------------------------------bean注入-------------------------------------
    @Autowired
    protected JdbcDao jdbcDao;

    protected JpaRepository<T, String> repo;

    public void setRepo(JpaRepository<T, String> repo) {
        this.repo = repo;
    }

}
