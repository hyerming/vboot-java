package vboot.core.common.utils.web;

import com.aspose.words.Document;
import com.aspose.words.SaveFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import vboot.core.common.utils.ruoyi.StringUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;

public class XresUtil {

    public static void downloadExcelXls(HttpServletResponse response, HSSFWorkbook workbook, String name) throws IOException {
        String fileName = URLEncoder.encode(name, "UTF-8");
        response.setHeader("content-disposition", "attachment;filename=" + fileName + ".xls");
        response.setContentType("application/vnd.ms-excel");
        OutputStream out = response.getOutputStream();
        workbook.write(out);
        out.flush();
        out.close();
    }

    public static void downloadExcelXlsx(HttpServletResponse response, XSSFWorkbook workbook, String name) throws IOException {
        String fileName = URLEncoder.encode(name, "UTF-8");
        response.setHeader("content-disposition", "attachment;filename=" + fileName + ".xlsx");
        response.setContentType("application/vnd.ms-excel");
        OutputStream out = response.getOutputStream();
        workbook.write(out);
        out.flush();
        out.close();
    }

    public static void downloadWord(Document doc, String docName, String formatType,
                                    boolean openNewWindow, HttpServletResponse response)
            throws Exception {
        docName = URLEncoder.encode(docName, "UTF-8");
        String extension = formatType;
        if (formatType.equals("WML") || formatType.equals("FOPC")) {
            extension = "XML";
        }

        String fileName = docName + "." + extension;

        if (openNewWindow) {
            response.setHeader("content-disposition", "attachment; filename=" + fileName);
        } else {
            response.addHeader("content-disposition", "inline; filename=" + fileName);

        }
        response.addHeader("Access-Control-Expose-Headers", "Content-Disposition,download-filename");
        response.setHeader("download-filename", fileName);

        if ("DOC".equals(formatType)) {
            response.setContentType("application/msword");
            doc.save(response.getOutputStream(), SaveFormat.DOC);
        } else if ("DOCX".equals(formatType)) {
            response.setContentType("application/vnd.openxmlformats-officedocument.wordprocessingml.document");
            doc.save(response.getOutputStream(), SaveFormat.DOCX);
        } else if ("PDF".equals(formatType)) {
            response.setContentType("application/pdf");
            doc.save(response.getOutputStream(), SaveFormat.PDF);
        }
        response.flushBuffer();
    }


    /**
     * 将word转换为pdf
     * @param inPath word存储路径
     * @param outPath pdf保存路径
     */
    public static void doc2pdf(String inPath, String outPath) {
        if (StringUtils.isEmpty(inPath) || StringUtils.isEmpty(outPath)) { // 验证License 若不验证则转化出的pdf文档会有水印产生
            return;
        }
        try {
            long old = System.currentTimeMillis();
            File file = new File(outPath); // 新建一个空白pdf文档
            FileOutputStream os = new FileOutputStream(file);
            Document doc = new Document(inPath); // Address是将要被转化的word文档
            doc.save(os, SaveFormat.PDF);// 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF,
            // EPUB, XPS, SWF 相互转换
            long now = System.currentTimeMillis();
            System.out.println("Word转换成功，共耗时：" + ((now - old) / 1000.0) + "秒"); // 转化用时
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * 将word转为pdf
     * @param input 需要转换的word
     * @param saveFile 保存pdf文件
     */
    public static void doc2pdf(InputStream input, File saveFile) {
        if (input == null || saveFile == null) { // 验证License 若不验证则转化出的pdf文档会有水印产生
            return;
        }
        try {
            long old = System.currentTimeMillis();
            FileOutputStream os = new FileOutputStream(saveFile);
            Document doc = new Document(input); // Address是将要被转化的word文档
            doc.save(os, SaveFormat.PDF);// 全面支持DOC, DOCX, OOXML, RTF HTML, OpenDocument, PDF,
            // EPUB, XPS, SWF 相互转换
            long now = System.currentTimeMillis();
            System.out.println("Word转换成功，共耗时：" + ((now - old) / 1000.0) + "秒"); // 转化用时
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
