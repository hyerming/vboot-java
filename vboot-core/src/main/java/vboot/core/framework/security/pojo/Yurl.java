package vboot.core.framework.security.pojo;

import lombok.Data;

//url扫描收集时是用到的pojo
@Data
public class Yurl {
    private String id ;

    private String url ;

    private String pid ;

    private String type;
}
