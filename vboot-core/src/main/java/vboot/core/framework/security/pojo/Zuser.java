package vboot.core.framework.security.pojo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serializable;
import java.util.Collection;

//用户pojo，前台与后台都会用到，前台用于获取用户信息，后台存在redis里，用于获取用户信息及权限校验
@Data
public class Zuser implements UserDetails, Serializable {

    private static final long serialVersionUID = -2487878824333265058L;

    private String id;//用户ID

    private String name;//用户姓名

    private String usnam;//用户名

    private String monum;//手机号

    private String label;//账号标签

    private String type;//账号类型

    private String depid;//部门id，协同用户则是公司id

    private String depna;//部门名称，协同用户则是公司名称

    private String relog;//前台通知是否查看过的标记

    private String avimg;//头像

    @JsonIgnore//前端不需要
    private long[] permArr;//权限集,用于验证URL权限

    @JsonIgnore//前端不需要
    private String conds;//组织架构集，用户ID，所有上级部门ID,岗位ID,群组ID

//    public Zuser(String id, String name, String usnam, long[] perms, String conds) {
//        this.id = id;
//        this.name = name;
//        this.usnam = usnam;
//        this.conds = conds;
//        this.permArr = perms;
//    }

//    public Zuser(String id, String name, String usnam) {
//        this.id = id;
//        this.name = name;
//        this.usnam = usnam;
//    }

    public Zuser() {

    }

    public Zuser(UserDo userDo) {
        this.id = userDo.getId();
        this.name = userDo.getName();
        this.usnam = userDo.getUsnam();
        this.monum = userDo.getMonum();
        this.label = userDo.getLabel();
        this.type = userDo.getType();
        this.depid = userDo.getDepid();
        this.depna = userDo.getDepna();
        this.relog = userDo.getRelog();
        this.avimg = userDo.getAvimg();
    }

    //------------------------------
    private boolean isAdmin;

    public boolean isAdmin() {
        if("sa".equals(usnam)||"admin".equals(usnam)||"vben".equals(usnam)){
            return true;
        }
        return isAdmin;
//        return true;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    //权限校验
    public boolean hasPerm(int pos, long code) {
        if (isAdmin()) {
            return true;
        }
        if (permArr.length <= pos) {
            return false;
        }
        long ret = permArr[pos] & code;
        return !(ret == 0);
    }

    //----------------------------UserDetails------------------------------------------------
    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return null;
    }

    @JsonIgnore
    @Override
    public String getUsername() {
        return this.getUsnam();
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public boolean isEnabled() {
        return true;
    }

    //@JSONField(serialize = false)
}
