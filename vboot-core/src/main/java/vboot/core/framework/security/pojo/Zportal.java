package vboot.core.framework.security.pojo;

import lombok.Data;

//前台返回门户
@Data
public class Zportal {
    private String id;
    private String name;
}
