package vboot.core.framework.security.pojo;

import lombok.Data;

//数据库用户信息，登录时用到
@Data
public class UserDo {

    private String id;

    private String name;

    private String usnam;

    private String pacod;

    private Boolean catag;

    private String tier;

    private String depid;

    private String depna;

    private String monum;

    private String label;

    private String type;

    private String relog;

    private String avimg;

}
