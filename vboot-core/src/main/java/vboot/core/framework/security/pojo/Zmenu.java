package vboot.core.framework.security.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

//前台返回菜单
@Data
public class Zmenu implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;

    private String pid;

    @JsonIgnore
    @JSONField(serialize = false)
    private String perm;

    private String path;

    private String name;

    private String component;

    private Zmeta meta;

    private String redirect;

    private List<Zmenu> children;

    private String type;

    private String porid;


}
