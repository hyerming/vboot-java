package vboot.core.framework.webservice;


import javax.jws.WebService;

//@AutoPublish(publishAddress = "/pig")
@WebService(name = "PigService", // 暴露服务名称
        targetNamespace = "http://service.sbmpservice.usts.edu.cn"// 命名空间,一般是接口的包名倒序
)
public interface PigService {

    int addUser();

}
